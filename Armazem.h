/* 
 * File:   Armazem.h
 * Author: Bruno
 *
 * Created on 5 de Outubro de 2014, 18:21
 */

#ifndef ARMAZEM_
#define	ARMAZEM_

#include "Deposito.h"
#include "DepositoFresco.h"
#include "DepositoNormal.h"
#include "graphStlPath.h"


class Armazem : public graphStlPath<Deposito*,int>{
    private:
        vector<Deposito* > depositos;
        int num_dep_normais;
        int num_dep_frescos;
        int num_paletes_normais;
        int num_paletes_frescas;
        vector<vector<double> > matriz;
        graphStlPath<Deposito*,int> grafo;
        //colocar matriz como atributo
        
    
    public:
        Armazem();
        //Armazem(vector<Deposito* > dep, int ndm,int ndf, int npn, int npf, int dist);
        Armazem(const Armazem& copia);
        Armazem(int min, int max);
        ~Armazem();
        
        vector<Deposito* > getDepositos() const;
        int getNumDepNormais() const;
        int getNumDepFrescos()const;
        int getNumPaletesNormais() const;
        int getNumPaletesFrescas() const;
        vector<vector<double> > getMatriz();
        
        void empilharDep (Deposito& d);
        void guardarEstruturaTxt();
        int NumActualDepFrescos();
        int NumActualDepNormais();
        
        void gerarMatriz();
        void gerarGrafo();
        void preencherMatriz(double ** mat);
        int randomNumber();
        
        void setDepositos(vector<Deposito* > dep);
        void setNumDepNormais(int ndm);
        void setNumDepFrescos(int ndf);
        void setNumPaletesNormais(int npn);
        void setNumPaletesFrescas(int npf);
        
        void escreve(ostream& out) const;
        
        void percursosPossiveis(int si, int sf);
        void menorCaminho(int si, int sf);
        void escreverFicheiro();
        void lerFicheiro();
        void caminhoMesmoTipoDeposito(int di, int df);
        void mostraCaminho(int vi, const vector<int> &path);

 
    
    
};


Armazem::Armazem():graphStlPath<Deposito*, int>() {    
        
        num_dep_normais= 0;
        num_dep_frescos=0;
        num_paletes_normais=0;
        num_paletes_frescas=0;
              
        
}

//Armazem::Armazem(vector<Deposito* > dep, int ndm, int ndf, int npn, int npf, int dist){
//        depositos=dep;
//        num_dep_normais=ndm;
//        num_dep_frescos=ndf;
//        num_paletes_normais=npn;
//        num_paletes_frescas=npf;
//        distancia=dist;
//}

Armazem::Armazem(const Armazem& copia):graphStlPath<Deposito*, int>(copia){
        depositos=copia.getDepositos();
        num_dep_normais=copia.getNumDepNormais();
        num_dep_frescos=copia.getNumDepFrescos();
        num_paletes_normais=copia.getNumPaletesNormais();
        num_paletes_frescas=copia.getNumPaletesFrescas();
}

Armazem::Armazem(int min, int max) : graphStlPath<Deposito*, int>(){
    int x=0;
        srand(time(NULL));
        num_dep_normais= rand() % max + min;
        num_dep_frescos=rand() % max + min;
        num_paletes_normais=rand() % max + min;
        num_paletes_frescas=rand() % max + min;

        
        for(int i = 0;i<num_dep_normais;i++){
            int cod = x;
            x++;
            int cap_max = rand() %50 + 1;
            int area = rand () % 50 + 1;
            vector<stack<int> > vec;
           
            for(int k = 0;k<num_paletes_normais;k++){
                stack<int> aux;
                vec.push_back(aux);
            }
            DepositoNormal *d = new DepositoNormal(cod,num_paletes_normais,cap_max,area,vec);
            this->addGraphVertex(d);
            
            depositos.push_back( new DepositoNormal(cod,num_paletes_normais,cap_max,area,vec) );
        }
        
        for(int j = 0;j<num_dep_frescos;j++){
            int cod = x;
            x++;
            int cap_max = rand() %50 + 1;
            int area = rand () % 50 + 1;
            vector<queue<int> > vec;
            
            for(int k = 0;k<num_paletes_normais;k++){
                queue<int> aux;
                vec.push_back(aux);
            }
            DepositoFresco *d = new DepositoFresco(cod,num_paletes_normais,cap_max,area,vec);
            this->addGraphVertex(d);
            
            depositos.push_back( new DepositoFresco(cod,num_paletes_frescas,cap_max,area,vec) );
        }
        
        this->gerarMatriz();
        this->gerarGrafo();
        
}

Armazem::~Armazem(){
    
}

vector<Deposito* > Armazem::getDepositos() const{
    return depositos;
}


int Armazem::getNumDepNormais() const{
    return num_dep_normais;
}


int Armazem:: getNumDepFrescos() const{
    return num_dep_frescos;
}


int Armazem::getNumPaletesNormais() const{
    return num_paletes_normais;
}


int Armazem::getNumPaletesFrescas() const{
    return num_paletes_frescas;
}

vector<vector<double> > Armazem::getMatriz() {
    return matriz;
}

void Armazem::setDepositos(vector<Deposito* >  dep){
    depositos=dep;
}


void Armazem::setNumDepNormais(int ndm){
    num_dep_normais=ndm;
}


void Armazem::setNumDepFrescos(int ndf){
    num_dep_frescos=ndf;
}


void Armazem::setNumPaletesNormais(int npn){
    num_paletes_normais=npn;
}

void Armazem:: setNumPaletesFrescas(int npf){
    num_paletes_frescas=npf;
}

void Armazem::empilharDep(Deposito& d){
    
    Deposito* p;
    int dep_frescos=this->NumActualDepFrescos();
    int dep_normais=this->NumActualDepNormais();
    
    
    if(depositos.size()==(num_dep_frescos+num_dep_normais)){
        
        cout<<"Armazem Cheio!!!"<<endl;
        return;
        
    }else{
        if(typeid(d)==typeid(DepositoFresco)){
           
            DepositoFresco& aux = dynamic_cast< DepositoFresco& >(d);
            vector<queue<int> > vec = aux.getPaletes();
            
            if(d.getNumPaletes()==num_paletes_frescas && vec.size()==num_paletes_frescas &&dep_frescos<num_dep_frescos ){
                p=&d;
                depositos.push_back(p);
                cout<<"Deposito adicionado com sucesso!!!"<<endl;
            }else{
               cout<<"Erro: Estrutura da palete incorrecta!!!"<<endl;
            }
            
        }else if(typeid(d)==typeid(DepositoNormal)){
            
            DepositoNormal& aux1 = dynamic_cast< DepositoNormal& >(d);
            vector<stack<int> > vec1 = aux1.getPaletes();
            
            if(d.getNumPaletes()==num_paletes_normais && vec1.size()==num_paletes_normais &&dep_normais<num_dep_normais){
                
                p=&d;
                depositos.push_back(p);
                cout<<"Deposito adicionado com sucesso!!!"<<endl;;
            
            }else{
            
                cout<<"Erro: Estrutura da palete incorrecta!!!"<<endl;
            }
        }
    }   
}


int Armazem::NumActualDepFrescos(){
    
    int cont=0;

    
    for(int i=0;i<depositos.size();i++){
        if(typeid(*depositos[i])==typeid(DepositoFresco)){
            cont++;
        }
    }
    return cont;
}


int Armazem::NumActualDepNormais(){
     
    int cont=0;
    
    for(int i=0;i<depositos.size();i++){
        if(typeid(*depositos[i])==typeid(DepositoNormal)){

            cont++;
        }
    }
 
    return cont;

}

void Armazem:: guardarEstruturaTxt(){
  ofstream myfile;
   myfile.open ("Armazem.txt");
   myfile <<"Armazem:\n";
   myfile << "Numero de Depositos Normais: " << num_dep_normais << "\nNumero de Depositos Frescos: "
             << num_dep_frescos << "\nNumero de Paletes Normais: "<< num_paletes_normais 
             <<"\nNumero de Paletes Frescas: " << num_paletes_frescas<<endl;
         int aux = 0;
    for(int i = 0; i<depositos.size();i++){
         if(aux > num_dep_normais){
            myfile <<"Deposito Fresco";
        }else{
        myfile << "Deposito Normal";
        }
         
        myfile<<(*depositos[i]);
        aux++;
    } 
         
         myfile<<"\n"<<endl;
         myfile<<"Matriz"<<endl;
         
         for (int i = 0; i < matriz.size(); i++) {
        for (int j = 0; j < matriz[i].size(); j++) {
            myfile   << matriz[i][j]<< " ";
        }
        myfile << " " << endl;
    }
             
   
   myfile.close();
 }


int Armazem::randomNumber() {
    int min = 1, max = 9, range;
    range = max - min + 1;
    return rand() % range + min;
}
 
 void Armazem::gerarMatriz() {
int numTotalDepositos = num_paletes_normais + num_paletes_frescas;
    for (int i = 0; i < numTotalDepositos; i++) {
        vector<double> temp;
        for (int j = 0; j < numTotalDepositos; j++) {
            temp.push_back(0);
        }
        matriz.push_back(temp);
    }

    for (int i = 0; i < matriz.size(); i++) {
        for (int j = 0; j < matriz[i].size(); j++) {
            if (matriz[i][j] == 0 && matriz[j][i] == 0 && i!=j) {
                matriz[i][j] = randomNumber();
                matriz[j][i] = matriz[i][j];
            }
        }
    }

    for (int i = 0; i < matriz.size(); i++) {
        for (int j = 0; j < matriz[i].size(); j++) {
            cout  << " " << matriz[i][j];
        }
        cout << " " << endl;
    }
 }

 void Armazem::gerarGrafo() {
   for (int i = 0; i < depositos.size(); i++) {
        this->addGraphVertex(depositos[i]);
    }
    for (int j = 0; j< depositos.size(); j++) {
        for (int k = 0; k< depositos.size(); k++) {
            this->addGraphEdge(matriz[j][k], depositos[j], depositos[k]);
        }
    }
 }

void Armazem::escreve(ostream& out) const 
{
    out<<"Armazem\n";
    
    out<<"Numero de Depositos Normais: " << num_dep_normais << "\nNumero de Depositos Frescos: "
            << num_dep_frescos << "\nNumero de Paletes Normais: "<< num_paletes_normais 
            <<"\nNumero de Paletes Frescas: " << num_paletes_frescas<<endl;
    
    
    
        int aux = 0;
    for(int i = 0; i<depositos.size();i++){
         if(aux > num_dep_normais){
            out <<"Deposito Fresco";
        }else{
        out << "Deposito Normal";
        }
         
        out<<(*depositos[i]);
        aux++;
    }
        
        out<<"\n"<<endl;
         out<<"Matriz"<<endl;
         
         for (int i = 0; i < matriz.size(); i++) {
        for (int j = 0; j < matriz[i].size(); j++) {
            out  << " " << matriz[i][j];
        }
        out << " " << endl;
    }
        
}


ostream& operator <<(ostream& out, const Armazem& a) {
    a.escreve(out);
    return out;
}

void Armazem::percursosPossiveis(int si, int sf) {
    cout << "Caminhos possiveis entre " << depositos[si]->getChave() << " e " << depositos[sf]->getChave() << ":" << endl;
    queue< stack<Deposito*> > qss = this->distinctPaths(depositos[si],depositos[sf]);
    while(!qss.empty()) {
        stack<Deposito*> st;
         
         while(!qss.front().empty()) {
             st.push(qss.front().top());
             qss.front().pop();
         }
        cout  << st.top()->getChave()<< " ";
        st.pop();
             while(!st.empty()) {
             cout << "-> " << st.top()->getChave() << " ";
             st.pop();
         }
         qss.pop();
         cout << endl;
    }
}

void Armazem::menorCaminho(int ci, int cf) {
        cout << "Caminhos Minimo entre " << depositos[ci]->getChave() << " e " << depositos[cf]->getChave() << ":" << endl;

    vector<int> path;
    vector<int> dist;
    int key;
    this->getVertexKeyByContent(key,depositos[cf]);
     this->dijkstrasAlgorithm(depositos[ci], path, dist);
     
        cout<< "Caminho mínimo:"<<dist[key]<<"m ;"<<endl;
        cout<<(*depositos[ci]).getChave();       
        mostraCaminho(key,path);

}

void Armazem::mostraCaminho(int vi, const vector<int> &path) {
    if (path[vi] == -1) {
        return;
    }
    mostraCaminho(path[vi], path);
    
    Deposito* c;
    this->getVertexContentByKey(c, vi);
    cout << " -> " << (*c).getChave();
}


void Armazem::lerFicheiro(){
    string linha;
    ifstream fx; //define variav. e abre fx. para leitura 
    fx.open("Armazem.txt");
    if (!fx) {
        cout << "Fx. nao existe !" << endl;
    }else{
    while (!fx.eof()) {
        getline(fx, linha, '\n');
        if (linha.size() > 0) {
            int pos = linha.find(':', 0);
            string line(linha.substr(0, pos));
            if (line == "Numero de Depositos Normais") {
                num_dep_normais = atoi((linha.substr(pos + 2, pos + 2)).c_str());
            } else if (line == "Numero de Depositos Frescos") {
                num_dep_frescos = atoi((linha.substr(pos + 2, pos + 2)).c_str());
            } else if (line == "Numero de Paletes Frescas") {
                num_paletes_frescas = atoi((linha.substr(pos + 2, pos + 2)).c_str());
            } else if (line == "Numero de Paletes Normais") {
                num_paletes_normais = atoi((linha.substr(pos + 2, pos + 2)).c_str());
            } else if (line == "Deposito Fresco") {
                vector<queue<int> > vec;
                int chave;
                getline(fx, linha, '\n');
                pos = linha.find(':', 0);
                string line1(linha.substr(0, pos));
                if(line1 == "Chave"){
                chave = atoi((linha.substr(pos + 2, pos + 2)).c_str());
                }
                int capMaxPaletes;
                int area;

                getline(fx, linha, '\n');
                getline(fx, linha, '\n');
                pos = linha.find(':', 0);
                string line2(linha.substr(0, pos));
                if (line2 == "Capacidade Máxima") {
                    capMaxPaletes = atoi((linha.substr(pos + 2, pos + 2)).c_str());
                }
                getline(fx, linha, '\n');
                pos = linha.find(':', 0);
                string line3(linha.substr(0, pos));
                if (line3 == "Area Total") {
                    area = atoi((linha.substr(pos + 2, pos + 2)).c_str());
                }
                depositos.push_back(new DepositoFresco(chave, num_paletes_frescas, capMaxPaletes, area,vec));
            } else if (line == "Deposito Normal") {
                vector<stack<int> > vec;
                int chave;
                getline(fx, linha, '\n');
                pos = linha.find(':', 0);
                string line1(linha.substr(0, pos));
                if(line1 == "Chave"){
                chave = atoi((linha.substr(pos + 2, pos + 2)).c_str());
                }
                int capMaxPaletes;
                int area;

                getline(fx, linha, '\n');
                getline(fx, linha, '\n');
                pos = linha.find(':', 0);
                string line2(linha.substr(0, pos));
                if (line2 == "Capacidade Máxima") {
                    capMaxPaletes = atoi((linha.substr(pos + 2, pos + 2)).c_str());
                }
                getline(fx, linha, '\n');
                pos = linha.find(':', 0);
                string line3(linha.substr(0, pos));
                if (line3 == "Area Total") {
                    area = atoi((linha.substr(pos + 2, pos + 2)).c_str());
                }
                depositos.push_back(new DepositoNormal(chave, num_paletes_normais, capMaxPaletes, area,vec));
            } else if (line == "Matriz") {
                int nDepositos = num_dep_frescos + num_dep_normais;

                int inic;
                getline(fx, linha, '\n');
                matriz.resize(nDepositos);
                for (int i = 0; i < nDepositos; i++) {
                    inic = 0;

                    matriz[i].resize(nDepositos);
                    for (int j = 0; j < nDepositos; j++) {
                        pos = linha.find(' ', inic);

                        matriz[i][j] = atoi((linha.substr(inic, pos - inic)).c_str());
                        pos++;
                        inic = pos;
                    }
                    getline(fx, linha, '\n');
                }
            }

        }
    }
    fx.close();
    this->gerarGrafo();
    }
}

void Armazem::caminhoMesmoTipoDeposito(int di, int df) {
  bool igual = false;
  Deposito* i = depositos[di];
  Deposito* f = depositos[df];

  if (i->getTipo() == f->getTipo()) {

     
      igual = true;
      queue<stack<Deposito*> > cam;
      cam = this->distinctPaths(depositos[di], depositos[df]);
      cout << "Caminho entre " << depositos[di]->getChave() << " e " << depositos[df]->getChave() << endl;
      if (cam.empty()){
          cout << "Nao exite caminho!" << endl;
      }
      bool igual;    
      while (!cam.empty()) {
          igual = true;
          stack<Deposito*>aux1;
          stack<Deposito*>aux = cam.front();
          while (!aux.empty() && igual == true) {
              Deposito* aux2 = aux.top();
              if (aux2->getTipo() == i->getTipo()) {
                  aux1.push(aux.top());
              } else {
                  igual = false;
              }
              aux.pop();
          }
          if (igual == true) {
              while (!aux1.empty()) {
                  cout << "-> " << aux1.top()->getChave() << " ";
                  aux1.pop();
              }
              break;
          }
      }
      if (igual == false) cout << "Não existe caminho!" << endl;
      
  }
  if (!igual) {
      cout << "O deposito final e inicial nao sao do mesmo tipo!" << endl;     
  }
}
#endif	/* ARMAZEM_ */

