/* 
 * File:   main.cpp
 * Author: Bruno
 *
 * Created on 29 de Setembro de 2014, 16:58
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include <queue>
#include <stack>
#include <map>
#include <fstream>
#include <time.h>
#include <typeinfo>
using namespace std;

#include "Armazem.h"
#include "DepositoFresco.h"
#include "DepositoNormal.h"
#include "Deposito.h"


/*
 * 
 */
int main(int argc, char** argv) {
    
    // vector<Deposito* > depositos=a.getDepositos();
     //for (int i=0; i<depositos.size(); i++){
       //  cout << depositos[i]->getChave()<< endl;
     //}
    //a.guardarEstruturaTxt();
    //a.lerFicheiro();
    //a.caminhoMesmoTipoDeposito(1,3);
//    cout<<a;
    

    bool sair = false;
    Armazem a = Armazem(2,2);
    while (sair == false) {
        cout << "\n**Simulador Armazém**"<< endl;

        cout << "1.Percursos possíveis entre depósitos." << endl;
        cout << "2.Percurso mínimo entre depósitos." << endl;
        cout << "3.Percursos possíveis entre depósitos do mesmo tipo tipo." << endl;
        cout << "4.Guardar e sair." << endl;
        cout << "5.Ler Ficheiro.\n" << endl;
        
        cout << "Seleccione a opção:" << endl;
        int op = 0;
        cin >> op;
        
        if (cin.fail()) {
            cout << "Opção não válida!" << endl;
        } else switch (op) {
                case 1:
                {
                    int d1,d2;
                    cout << "Insera o índice do depósito de origem:" << endl;
                    cin >> d1;
                    cout << "Insera o índice do depósito de destino:" << endl;
                    cin >> d2;
                    
                    a.percursosPossiveis(d1,d2);
                }
                    break;

                case 2:
                {
                      int d1,d2;
                    cout << "Insera o índice do depósito de origem:" << endl;
                    cin >> d1;
                    cout << "Insera o índice do depósito de destino:" << endl;
                    cin >> d2;
                    a.menorCaminho(d1,d2);
                    }
                    break;

                case 3:
                {
                    int d1,d2;
                    cout << "Insera o índice do depósito de origem:" << endl;
                    cin >> d1;
                    cout << "Insera o índice do depósito de destino:" << endl;
                    cin >> d2;
                    a.caminhoMesmoTipoDeposito(d1,d2);
                }
                    break;

                case 4:
                {
                    a.guardarEstruturaTxt();
                    cout << "A sair" << endl;
                    sair = true;
                }
                    break;
                    
                case 5:
                {
                    a.lerFicheiro();
                }
                    break;

                default:
                {
                    cout << "Opção não válida!" << endl;
                }
                    break;
            }
    }
    
    return 0;
   }
