/* 
 * File:   DepositoNormal.h
 * Author: Bruno
 *
 * Created on 5 de Outubro de 2014, 18:33
 */

#ifndef DEPOSITONORMAL_
#define	DEPOSITONORMAL_

#include "Deposito.h"


class DepositoNormal: public Deposito{
        private:
            vector<stack<int> > paletes;
        
        public:
    
            DepositoNormal();
            DepositoNormal(int cv,int n_pal, int cap, int area, vector<stack<int> > pal);
            DepositoNormal(const DepositoNormal& copia);
            ~DepositoNormal(){}
        
            void setPaletes(vector<stack<int> > pl);
            vector<stack<int> > getPaletes() const;     
            
            void empilhar(int p);
            void empilhar(stack<int> s );
            bool depositoCheio();
            void desempilhar();
            
            bool operator == (const DepositoNormal& d);
            DepositoNormal& operator = (const DepositoNormal& d);
            bool operator > (const DepositoNormal& d);
            
            void escreve(ostream& out) const;
            
};


DepositoNormal::DepositoNormal():Deposito(){
    this->setTipo(0);
}


DepositoNormal::DepositoNormal(int cv, int n_pal, int cap, int area,vector<stack<int> > pal):Deposito(cv,n_pal,cap,area){
    paletes=pal;
    this->setTipo(0);
    
}


DepositoNormal::DepositoNormal(const DepositoNormal& copia):Deposito(copia){
    
    paletes=copia.getPaletes();
    
}



void DepositoNormal::setPaletes(vector<stack<int> > pl){
    paletes=pl;
}


vector<stack<int> > DepositoNormal::getPaletes()const{
    return paletes;
}


void DepositoNormal::empilhar(int p){
    
    int max = this->getCapMax();
    int min = max/2;

    if(paletes.size()==0){
        cout<<"Não existem paletes neste deposito"<<endl;
        return ;
    }
    
    
    if(depositoCheio()){
        cout<<"Deposito Cheio: Impossível empilhar!"<<endl;
        return;
    }
    
    for(int i=0;i<paletes.size();i=i+2){

        if(paletes[i].size()< max  ){
            paletes[i].push(p);
            cout<<"Acrescentado numa palete par!!!"<<endl;
            return;
        }
    }
    
  
    for(int j=1;j<paletes.size();j=j+2){

        if(paletes[j].size()< min  ){   
            cout<<"Acrescentado numa palete impar!!!"<<endl;
            paletes[j].push(p);
            return;
        }
       
    }
    
}



void DepositoNormal::empilhar(stack<int> s){
    
    int max = this->getCapMax();
    int min = max/2;
    
    while(!s.empty()){
        
    int p = s.top();

    empilhar(p);
        
    s.pop();
    
    }
    
    
}


bool DepositoNormal::depositoCheio(){
    int min = (this->getCapMax())/2;
    int par = (paletes.size())-1;
    int impar = (paletes.size())-2;
    
   
    
    if (paletes.size()%2==0){
        
       if((int)(paletes[par].size())==min){
           return true;
        
       }else{
           
           return false;
        }
        
    }else{
        
        if((int)(paletes[impar].size())==min){
            return true;
       
        }else{
 
           return false;
        }
        
    }

}


void DepositoNormal::desempilhar(){
    
    if(paletes.empty()){
        cout<<"O deposito não tem produtos para desempilhar"<<endl;
        return;
    }
    
    if (paletes.size()%2==0){
    
    for(int j=(paletes.size()-1);j>-1;j=j-2){
        if(!paletes[j].empty()){
            while(!paletes[j].empty()){
                paletes[j].pop();
                cout<<"Retira Impar"<<endl;
            }
        }
    }
    
    for(int i=(paletes.size()-2);i>-1;i=i-2){
        if(!paletes[i].empty()){
            while(!paletes[i].empty()){
                paletes[i].pop();
                cout<<"Retira Par"<<endl;
            }
        }
    }
    
    }else{
        
        for(int j=(paletes.size()-2);j>-1;j=j-2){
        if(!paletes[j].empty()){
            while(!paletes[j].empty()){
                paletes[j].pop();
                cout<<"Retira Impar"<<endl;
            }
        }
    }
    
    for(int i=(paletes.size()-1);i>-1;i=i-2){
        if(!paletes[i].empty()){
            while(!paletes[i].empty()){
                paletes[i].pop();
                cout<<"Retira Par"<<endl;
        
            }
        }  
    }

        
        }
    
    for(int i=0;i<paletes.size();i++){
        if(!paletes[i].empty()){
            cout<<"Erro: Não foi possivel desempilhar"<<endl;
            return;
            }
        }
    
    cout<<"Sucesso!!!!"<<endl;
}  




void DepositoNormal::escreve(ostream& out) const 
{
    out<<"Deposito Normal:\n"<<endl;
    Deposito::escreve(out);
    vector<stack<int> > aux = paletes;
    vector<stack<int> >::iterator it;
    stack<int> temp;
    int i=1;
    
    for(it=aux.begin();it!=aux.end();it++){
        temp = *(it);
    while (!temp.empty()){
        out<<"Palete "<<i<<"\n"<<endl;
        out<<temp.top()<<'\n';
        temp.pop();
        i++;
        }
    }
}


bool DepositoNormal::operator == (const DepositoNormal& d){
       
    if(Deposito::operator ==(d)){
        if(paletes==d.paletes){
            return true;
        }
    }
       
    return false;
}


bool DepositoNormal::operator > (const DepositoNormal& d){
    
    if(Deposito::operator >(d)){
        if(this->getArea() > d.getArea()){
            return true;
        }
    }
    
    return false;
    
}


DepositoNormal& DepositoNormal::operator = (const DepositoNormal& d) {
    
    if(this != &d) {      
        Deposito::operator = (d);
        setPaletes(d.paletes);
    }
    
    return *this;

}


ostream& operator <<(ostream& out, const DepositoNormal& d) {
    d.escreve(out);
    return out;
}

#endif	/* DEPOSITONORMAL_ */