/* 
 * File:   DepositoFresco.h
 * Author: Bruno
 *
 * Created on 5 de Outubro de 2014, 18:33
 */

#ifndef DEPOSITOFRESCO_
#define	DEPOSITOFRESCO_

#include "Deposito.h"


class DepositoFresco: public Deposito {
    private:
       vector<queue<int> > paletes;
        
    public:
        
        DepositoFresco();
        DepositoFresco(int cv,int n_pal, int cap, int area, vector<queue<int> > pal);
        DepositoFresco(const DepositoFresco& copia);
        ~DepositoFresco(){}
        
        void setPaletes(vector<queue<int> > pl);
        
        vector<queue<int> > getPaletes()const;   
        
        void empilhar(int p);
        void empilhar(queue<int> s );
        void desempilhar();
        
        DepositoFresco& operator = (const DepositoFresco& df);
        bool operator < (const DepositoFresco& df);
        bool operator > (const DepositoFresco& df);
        bool operator == (const DepositoFresco& df);
        
        void escreve(ostream& out) const;
            
};

DepositoFresco::DepositoFresco():Deposito(){
    this->setTipo(1);
}

DepositoFresco::DepositoFresco(int cv, int n_pal, int cap, int area, vector<queue<int> > pal):Deposito(cv,n_pal,cap,area){
    paletes=pal;
    this->setTipo(1);
    
}


DepositoFresco::DepositoFresco(const DepositoFresco& copia):Deposito(copia){
    paletes=copia.getPaletes();
}


void DepositoFresco::setPaletes(vector<queue<int> > pl){
    paletes=pl;
}


vector<queue<int> > DepositoFresco::getPaletes() const{
    return paletes;
}

void DepositoFresco::escreve(ostream& out) const 
{
    out<<"Deposito Fresco:\n"<<endl;
    Deposito::escreve(out);
    vector<queue<int> > aux = paletes;
    vector<queue<int> >::iterator it;
    queue<int> temp;
    int i=1;
    
    for(it=aux.begin();it!=aux.end();it++){
        temp = *(it);
    while (!temp.empty()){
        out<<"Palete "<<i<<"\n"<<endl;
        out<<temp.front()<<'\n';
        temp.pop();
        i++;
        }
    }
}


void DepositoFresco::empilhar(int p) {
int palete_menor=this->getCapMax();
    
    for(int i=0;i<paletes.size();i++){
        if(paletes[i].size()<paletes[palete_menor].size()){
            palete_menor=i;
        }
    }
    if (paletes[palete_menor].size()>=this->getCapMax()-1){
        cout<<"O deposito fresco está cheio!"<<endl;
    }else{
        paletes[palete_menor].push(p);
        cout<<"Acrescentado na palete " << palete_menor << "!!!" << endl;
    }
    
}

void DepositoFresco::desempilhar(){
    
    if(paletes.empty()){
        cout<<"O deposito não tem produtos para desempilhar"<<endl;
        return;
    }else{
        for (int i=0;i<paletes.size();i++){
                paletes[i].pop();
        }
}
    for(int k=0;k<paletes.size();k++){
        if(paletes[k].empty()){
            cout<<"Palete "<<k<<" ficou vazia."<<endl;
            }
        }
}


void DepositoFresco::empilhar(queue<int> produtos){
    while(!produtos.empty()){
    int p = produtos.front();
    empilhar(p);     
    produtos.pop();  
    }
}

bool DepositoFresco::operator ==(const DepositoFresco& df) {
    if(paletes.size()==df.getPaletes().size()){
        return true;
    }else{
        return false;
    }
}

ostream& operator <<(ostream& out, const DepositoFresco& d) {
    d.escreve(out);
    return out;
}



#endif	/* DEPOSITOFRESCO_ */