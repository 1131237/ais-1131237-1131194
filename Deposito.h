/* 
 * File:   Deposito.h
 * Author: Bruno
 *
 * Created on 5 de Outubro de 2014, 18:25
 */

#ifndef DEPOSITO_
#define	DEPOSITO_

#include "Armazem.h"


class Deposito {
    private:
        int chave;
        int num_paletes;
        int cap_max;
        int area_total;
        int tipo; //0 - Normal, 1 - Fresco
    
    public:
        Deposito();
        Deposito(int cv,int paletes, int cap, int area);
        Deposito(const Deposito& copia);
        virtual ~Deposito();
        
        int getChave() const;
        int getNumPaletes()const;
        int getCapMax() const;
        int getArea() const;
        int getTipo() const;
       
        
        void setChave(int cv);
        void setNumPaletes(int paletes);
        void setCapMax(int cap);
        void setArea(int area);
        void setTipo(int tipo);
       
        virtual void empilhar(int p)=0;
        
        bool operator == (const Deposito& d);
        Deposito& operator = (const Deposito& d);
        bool operator > (const Deposito& d);
        
        void escreve(ostream& out) const;
        
    


};


Deposito::Deposito(){
    chave=0;
    num_paletes=0;
    cap_max=0;
    area_total=0;
}


Deposito::Deposito(int cv, int paletes, int cap, int area){
    chave=cv;
    num_paletes=paletes;
    cap_max=cap;
    area_total=area;
}


Deposito::Deposito(const Deposito& copia){
    chave=copia.getChave();
    num_paletes=copia.getNumPaletes();
    cap_max=copia.getCapMax();
    area_total=copia.getArea();
}


Deposito::~Deposito(){
    
}


int Deposito::getChave() const{
    return chave;
}
int Deposito :: getTipo() const {
        return tipo;
    }

void Deposito :: setTipo(int tipo) {
        this->tipo = tipo;
    }

int Deposito::getNumPaletes() const{
    return num_paletes; 
}


int Deposito::getCapMax() const{
    return cap_max;
}


int Deposito::getArea() const{
    return area_total;
}

void Deposito::setChave(int cv){
    chave=cv;
}


void Deposito::setNumPaletes(int paletes){
    num_paletes=paletes;
}


void Deposito::setCapMax(int cap){
    cap_max=cap;
}


void Deposito::setArea(int area){
    area_total=area;
}


bool Deposito::operator == (const Deposito& d){
       
    if(chave==d.chave){
        return true;
    }
       
    return false;
}


bool Deposito::operator > (const Deposito& d){
    
    if(area_total>d.area_total){
        return true;
    }
    
    return false;
    
}


Deposito& Deposito::operator = (const Deposito& d) {
    
    if(this != &d) {      
    
        setChave(d.chave);
        setNumPaletes(d.num_paletes);
        setCapMax(d.cap_max);
        setArea(d.area_total);
        
    }
    
    return *this;

}




void Deposito::escreve(ostream& out) const 
{
    
    
    
    out << "\nChave: " << chave << "\nNumero de Paletes: " << num_paletes 
            << "\nCapacidade Máxima: " << cap_max << "\nArea Total: "<< area_total <<"\n"<< endl;
}

ostream& operator <<(ostream& out, const Deposito& d) {
    d.escreve(out);
    return out;
}


#endif	/* DEPOSITO_ */

